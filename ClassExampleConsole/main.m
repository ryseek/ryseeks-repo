//
//  main.m
//  ClassExampleConsole
//
//  Created by Руслашка  on 28.09.14.
//  Copyright (c) 2014 RKcompany. All rights reserved.
//
//  Заранее извиняюсь за глупый код. Некоторую инфу брал из книги Мэтта Нейбурга "Программирование для iOS 7", о которой вроде плохие отзывы.

#import <Foundation/Foundation.h>
#import "Book.h" // Класс назвал неправильно, как изменить название, если уже создал и задействовал? Если везде изменить RSBook, то ругается на что-то загадочное.
#import "Fantasy.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
  
        Book * newBook =[[Book alloc]init];
        Fantasy * fanBook = [[Fantasy alloc] init];
        
        [newBook set_name:@"Детская книга"]; // тут типо сеттер
        [newBook set_author:@"Борис Акунин"];
        [newBook set_pages:414];
        
        
        [fanBook set_name:@"Игра эндера"];
        [fanBook set_author:@"Орсон Скотт Кард"];
        [fanBook set_pages:414];
        
        
        //[newBook pagesCount:[newBook _pages] witnName:[newBook _name] withAuthor: [newBook _author]];
        [newBook writeInfo:newBook];
        [fanBook writeInfo:fanBook];
        
        NSLog(@"(Тест геттера)количество страниц:%i",[newBook _pages]);
        
    }
    return 0;
}
