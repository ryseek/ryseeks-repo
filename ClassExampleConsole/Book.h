//
//  Book.h
//  ClassExampleConsole
//
//  Created by Руслашка  on 28.09.14.
//  Copyright (c) 2014 RKcompany. All rights reserved.
//
// Это первый в моей жизни класс

#import <Foundation/Foundation.h>

@interface Book : NSObject{
    int pages;
    NSString * name;
    NSString * author;
}
@property(nonatomic) int _pages;
@property(nonatomic) NSString * _name;
@property(nonatomic) NSString * _author;
-(void) writeInfo: (Book *) Bookwr;
@end
